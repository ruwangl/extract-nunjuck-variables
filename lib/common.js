const fs = require('node:fs');
const path = require('node:path');

const artifactDir = path.join('', __dirname, '..', 'artifacts');

// Log file
const logFileName = `${getBaseFileName(require.main.filename)}_${new Date()
  .toISOString()
  .replace(/:/g, '-')}.log`;
const logDir = path.join(artifactDir, 'logs');
const logFilePath = path.join(logDir, logFileName);

// Report file
const reportFileName = `report.${getBaseFileName(
  require.main.filename
)}_${new Date().toISOString().replace(/:/g, '-')}.json`;
const reportDir = path.join(artifactDir, 'reports');
const reportFilePath = path.join(reportDir, reportFileName);

buildLogFilePath();
buildReportFilePath();

function buildLogFilePath() {
  if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir, { recursive: true });
  }
}

function buildReportFilePath() {
  if (!fs.existsSync(reportDir)) {
    fs.mkdirSync(reportDir, { recursive: true });
  }
}

function getBaseFileName(fileName) {
  return path.basename(fileName).split('.').slice(0, -1).join('.');
}

// Log function
const log = message => {
  const logMessage = `[${new Date().toISOString()}]: ${message}`;
  fs.appendFileSync(logFilePath, `${logMessage}\n`);
  // eslint-disable-next-line no-console
  console.log(logMessage);
};

// Report function
const writeToReport = report => {
  fs.appendFileSync(reportFilePath, report);
};

module.exports = {
  log,
  writeToReport
};
