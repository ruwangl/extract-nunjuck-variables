const { MongoClient } = require("mongodb");

const { log } = require("./common");

let mongoClient = null;

const connect = async (connectionString) => {
  if (mongoClient) {
    log("Using existing DB connection");
    return mongoClient;
  }

  log("Connecting to DB");
  const client = new MongoClient(connectionString);
  mongoClient = await client.connect();

  return mongoClient;
};

const disconnect = () => {
  if (!mongoClient) {
    log("No DB connection to disconnect from");
    return;
  }

  log("Disconnecting from DB");

  return mongoClient.close();
};

const getTemplateCollection = (client, collectionName) => {
  return client.db().collection(collectionName);
};

const getTemplatesByName = async (client, name, collectionName) => {
  const templates = await getTemplateCollection(client, collectionName)
    .find({
      name,
    })
    .toArray();
  return templates;
};

const getAllTemplates = async (client, collectionName) => {
  const templates = await getTemplateCollection(client, collectionName).find().toArray();
  return templates;
};

module.exports = {
  connect,
  disconnect,
  getTemplatesByName,
  getAllTemplates,
};
