/**
 * Thank you Copilot for writing some of the code for me
 */

const { compiler } = require("nunjucks");
const { parseScript } = require("esprima");

// Get Code string which can parse to generate AST
function getCode(template) {
  const op = compiler.compile(template);

  return `function outerWrapper() {
    ${op}
  }`;
}

// Traverse AST to extract variables
const traverse = (node, variables = [], assignment = {}) => {
  for (const key in node) {
    if (key in node) {
      const child = node[key];
      if (typeof child === "object" && child) {
        // ExpressionStatement
        if (child.type === "AssignmentExpression") {
          if (child.operator === "=") {
            const left = child.left;
            const right = child.right;
            if (
              left.type === "Identifier" &&
              right.type === "ObjectExpression"
            ) {
              const expressionStatement = extractPropertyValues(
                left.name,
                right.properties
              );
              const keys = Object.keys(expressionStatement);
              if (keys.length > 0) {
                for (const key of keys) {
                  assignment[key] = expressionStatement[key];
                }
              }
            }
          }
        }

        // Extract default properties
        if (child.type === "BlockStatement") {
          if (Array.isArray(child.body)) {
            const expressionStatement = child.body[0];
            const expression = expressionStatement.expression;
            if (
              expression &&
              expression.callee?.object?.name === "context" &&
              expression.callee?.property?.name === "setVariable"
            ) {
              const expressionArguments = expression.arguments;
              if (Array.isArray(expressionArguments)) {
                const literals = extractLiterals(expressionArguments);
                if (literals.length > 0) {
                  const Identifier = expressionArguments[1];
                  if (Identifier && Identifier.type === "Identifier") {
                    if (assignment[Identifier.name]) {
                      variables.push({
                        [literals[0]]: assignment[Identifier.name],
                      });
                    } else {
                      variables.push({
                        [literals[0]]:
                          "#### unable to locate default values ###",
                      });
                    }
                  }
                }
              }
            }
          }
          // ?.callee?.property?.name === 'setVariable'
        }

        // SuppressValue
        if (child.callee?.property?.name === "suppressValue") {
          // Get SuppressValue arguments
          if (Array.isArray(child.arguments)) {
            const memberLookupExpression = child.arguments[0];
            if (
              memberLookupExpression?.callee?.property?.name === "memberLookup"
            ) {
              // Get MemberLookup arguments
              if (Array.isArray(memberLookupExpression.arguments)) {
                // Recursively get literals
                const literals = extractLiterals(
                  memberLookupExpression.arguments
                );
                variables.push(literals.join("."));
              }
            } else if (
              memberLookupExpression?.callee?.property?.name ===
              "contextOrFrameLookup"
            ) {
              // Get ContextOrFrameLookup arguments
              if (Array.isArray(memberLookupExpression.arguments)) {
                const literals = extractLiterals(
                  memberLookupExpression.arguments
                );
                variables.push(literals.join("."));
              }
            }
          }
        }

        if (Array.isArray(child)) {
          child.forEach((subNode) => traverse(subNode, variables, assignment));
        } else {
          traverse(child, variables, assignment);
        }
      }
    }
  }

  return variables;
};

// Extract property values
function extractPropertyValues(name, node) {
  const propertyValues = {};

  function traversePropertyValues(node) {
    if (node && typeof node === "object") {
      if (node.type === "Property") {
        if (!propertyValues[name]) {
          propertyValues[name] = {};
        }

        if (node.key.type === "Literal") {
          const variableProperty = propertyValues[name];
          if (node.value.type === "Literal") {
            variableProperty[node.key.value] = node.value.value;
          } else if (node.value.type === "CallExpression") {
            const literals = extractLiterals(node.value.arguments);
            variableProperty[node.key.value] = literals[0];
          }
        }

        // If the value itself has a type Property, traverse it as well
        if (node.value.type === "Property") {
          traversePropertyValues(node.value);
        }
      }

      Object.values(node).forEach((value) => traversePropertyValues(value));
    }
  }

  traversePropertyValues(node);
  return propertyValues;
}

// Extract literals
function extractLiterals(node) {
  const literals = [];

  function traverseLiterals(node) {
    if (node && typeof node === "object") {
      if (node.type === "Literal") {
        literals.push(node.value);
      }
      Object.values(node).forEach((value) => traverseLiterals(value));
    }
  }

  traverseLiterals(node);
  return literals;
}

// Get variables
const getVariables = (template) => {
  const code = getCode(template);
  const ast = parseScript(code);
  return traverse(ast);
};

module.exports = {
  getVariables,
};
