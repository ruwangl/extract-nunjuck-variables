require('dotenv').config();

const { log, writeToReport } = require('./lib/common');
const { connect, disconnect, getAllTemplates } = require('./lib/mongo');
const { getVariables } = require('./lib/extract-variables');

const emailMongoDbConnectionString = process.env.EMAIL_DB_MONGO_CLIENT;
const emailTemplateCollection = process.env.EMAIL_TEMPLATE_COLLECTION;

function getVariablesFrom(_id, text, from) {
  try {
    return getVariables(text);
  } catch (error) {
    process.stdout.write('\n');
    log(`[getVariablesFrom][${from}][${_id}]: ${error}`);
    return null;
  }
}

async function run(mongoConnectionString) {
  const client = await connect(mongoConnectionString);
  try {
    const templates = await getAllTemplates(client, emailTemplateCollection);
    log(`Found ${templates.length} templates`);
    const report = [];
    for (const { _id, name, subject, message } of templates) {
      try {
        const messageVariables = getVariablesFrom(_id, message, 'from-message');
        const subjectVariables = getVariablesFrom(_id, subject, 'from-subject');

        report.push({
          _id,
          templateName: name,
          subjectVariables,
          messageVariables
        });
      } catch (error) {
        log(`[In template][${_id}][${name}]: ${error}`);
        log(error);
      }
      process.stdout.write('.');
    }
    writeToReport(JSON.stringify(report, null, 2));
  } catch (error) {
    log(error);
  } finally {
    process.stdout.write('\n');
    await disconnect();
  }
}

// eslint-disable-next-line no-console
run(emailMongoDbConnectionString).catch(console.error);
