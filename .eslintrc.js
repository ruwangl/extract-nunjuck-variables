module.exports = {
  env: {
    es6: true,
    node: true
  },
  extends: ['eslint:recommended'],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parserOptions: {
    ecmaVersion: 2021,
    sourceType: 'module'
  },
  rules: {
    'prefer-spread': 0,
    'no-console': 2,
    curly: 0,
    'no-unused-vars': 0,
    eqeqeq: 1
  }
};
